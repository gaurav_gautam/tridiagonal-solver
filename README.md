Tridiagonal solver
==================

This program implements the algorithm for solving tridiagonal linear system of 
equations in parallel published by Nathan Mattor, Timothy J. Williams, and 
Dennis W. Hewett of Lawrence Livermore National Laboratory. Their paper can be
found [here](http://www.mcs.anl.gov/~zippy/publications/partrid/partrid.html).

This is a c++ code that will be tested on the hpc2010 cluster of Indian 
Institute of Technology, Kanpur. This program is being developed as a course 
project for the High Performance Computing (PHY690X) offered in 2015.

You will need to have blitz installed to compile this program.

Compilation and running
-----------------------

1. Clone the repo
2. Checkout to `linear-solver` branch
3. Compile with: `mpicxx main.cc serialsolver.cc parallelsolver.cc`
4. run with: `mpirun -np 5 ./a.out N` where N is the size of matrix on the 4 slave processors.
5. In case you don't want to get a lot of prints, checkout to the branch `commandline-args` and follow the same instructions as above. 